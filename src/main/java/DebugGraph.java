import Elements.Element2D;
import Elements.Position;

import java.awt.*;
import java.util.LinkedList;

public class DebugGraph extends Element2D
{
    private LinkedList<Long> data;
    private String name;

    Position pos = new Position();

    public DebugGraph(LinkedList<Long> data, long width, String name)
    {
        this.data = data;
        this.name = name;
    }

    public void setPos(Position pos)
    {
        this.pos = pos;
    }

    public void setPos(int x, int y)
    {
        this.pos.x = x;
        this.pos.y = y;
    }

    @Override
    public void render(Graphics g)
    {
        Long maxFPS = Long.valueOf(1);
        for (Long fps : data)
        {
            if (maxFPS < fps)
            {
                maxFPS = fps;
            }
        }

        Color oldColor = g.getColor();
        g.setColor(Color.GRAY);
        g.fillRect(pos.x, pos.y, 370, 150);
        g.setColor(oldColor);

        float multiplier = maxFPS / 100.f;
        for (int i = 0; i < data.size(); ++i)
        {
            int y = (int) (pos.y + 125 - (data.get(i) / multiplier));
            int x = 20 + i;

            g.drawLine(x, y, x, y);
        }

        g.drawString(name, pos.x + 10, pos.y + 15);
        g.drawString("- 0", pos.x + 310, pos.y + 129);
        g.drawString("- " + maxFPS, pos.x + 310, pos.y + 24);
        g.drawString("- " + (maxFPS / 2), pos.x + 310, pos.y + 78);

        g.drawRect(pos.x, pos.y, 370, 150);
        g.drawRect(pos.x + 10, pos.y + 20, 300, 105);
    }
}
