import Elements.RelativePosition;
import model.Spaceship;
import model.Sun;
import model.UnitImage;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Random;
import java.util.Vector;

public class Main
{
    private static final GamePanel panel = new GamePanel(null, null);
    private static final GameWindow window = new GameWindow(panel);
    private static final DebugPanel debugPanel = new DebugPanel();

    private static long lastUpdate = System.nanoTime();
    private static final long UPS = 60;
    private static final long updateTime = 1000000000 / UPS;

    private static GamePanel.SynchronizedBuffer buffer0 = new GamePanel.SynchronizedBuffer();
    private static GamePanel.SynchronizedBuffer buffer1 = new GamePanel.SynchronizedBuffer();
    private static int currentBuffer = 0;

    private static final Spaceship testShip = new Spaceship(UnitImage.InstanceType.SPACESHIP_WHITE);

    private static final Vector<Sun> suns = new Vector<>();
    private static final Vector<Spaceship> spaceships = new Vector<>();

    /*
     ** sets up the two buffers
     */
    private static void setupBuffers()
    {
        Runnable r = () ->
        {
            buffer0.buffer = new BufferedImage(window.getWidth(), window.getHeight(), BufferedImage.TYPE_INT_RGB);
            buffer1.buffer = new BufferedImage(window.getWidth(), window.getHeight(), BufferedImage.TYPE_INT_RGB);
            RelativePosition.setWindowSize(window.getWidth(), window.getHeight());
            UnitImage.setWindowSize(window.getWidth(), window.getHeight());
        };
        r.run();

        panel.setBuffers(buffer0, buffer1);
        window.setResizeListener(r);
    }

    /*
     ** processes user input
     */
    private static void processInput()
    {
        debugPanel.updateFPS();
        float timeMulti = debugPanel.getTimeMultiplier();

        RelativePosition shipPos = new RelativePosition();
        shipPos.setX(testShip.getRelativePos().getX() + (0.002f * timeMulti));
        shipPos.setY(testShip.getRelativePos().getY() + (0.002f * timeMulti));
        testShip.setRelativePos(shipPos);
        for (Sun sun : suns)
        {
            spaceships.addAll(Arrays.asList(sun.produceSpaceships(timeMulti)));
        }
    }

    /*
     ** Draws everything into screen buffer
     */
    private static void draw(Graphics g, GamePanel.SynchronizedBuffer buffer)
    {
        Random r = new Random();
        g.clearRect(0, 0, buffer.buffer.getWidth(), buffer.buffer.getHeight());
        testShip.render(g);
        debugPanel.render(g);
        for (Sun sun : suns)
        {
            sun.render(g);
        }
        for (Spaceship spaceship : spaceships)
        {
            spaceship.render(g);
        }
        g.dispose();
    }

    /*
     ** renders screen
     */
    private static void render()
    {
        GamePanel.SynchronizedBuffer buffer = (currentBuffer == 0) ? buffer1 : buffer0;

        buffer.mutex.enter();
        draw(buffer.buffer.getGraphics(), buffer);
        buffer.mutex.leave();

        panel.setCurrentBuffer(currentBuffer);
        currentBuffer = (currentBuffer + 1) % 2;
        panel.repaint();
    }

    public static void main(String[] args)
    {
        suns.add(new Sun(UnitImage.InstanceType.SUN_1_RED));
        suns.add(new Sun(UnitImage.InstanceType.SUN_1_BLUE));

        suns.get(0).setRelativePos(new RelativePosition(0.45f, 0.15f));
        suns.get(1).setRelativePos(new RelativePosition(0.45f, 0.65f));

        setupBuffers();
        while (true)
        {
            processInput();
            render();
        }
    }
}
