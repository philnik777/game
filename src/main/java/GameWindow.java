import javax.swing.*;

public class GameWindow extends JFrame
{
    private Runnable resizeEventListener = null;

    public GameWindow(GamePanel panel)
    {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel);
        setSize(300, 300);
        setVisible(true);
        setIgnoreRepaint(true);
    }

    public void setResizeListener(Runnable e)
    {
        resizeEventListener = e;
    }

    @Override
    public void validate()
    {
        super.validate();
        if (resizeEventListener != null)
        {
            resizeEventListener.run();
        }
    }
}
