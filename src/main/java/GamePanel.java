import com.google.common.util.concurrent.Monitor;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class GamePanel extends JPanel
{
    public static class SynchronizedBuffer
    {
        public BufferedImage buffer;
        public Monitor mutex = new Monitor();
    }

    private int currentBuffer = 0;
    private SynchronizedBuffer buffer0;
    private SynchronizedBuffer buffer1;

    public GamePanel(SynchronizedBuffer buffer0, SynchronizedBuffer buffer1)
    {
        setBackground(Color.BLACK);
        this.buffer0 = buffer0;
        this.buffer1 = buffer1;
    }

    public void setBuffers(SynchronizedBuffer buffer0, SynchronizedBuffer buffer1)
    {
        this.buffer0 = buffer0;
        this.buffer1 = buffer1;
    }

    @Override
    public void paintComponent(Graphics g)
    {
        SynchronizedBuffer buffer = getCurrentBuffer();
        buffer.mutex.enter();
        g.drawImage(buffer.buffer, 0, 0, this);
        buffer.mutex.leave();
    }

    public void setCurrentBuffer(int currentBuffer)
    {
        this.currentBuffer = currentBuffer;
    }

    public SynchronizedBuffer getCurrentBuffer()
    {
        return (currentBuffer == 0) ? buffer0 : buffer1;
    }
}
