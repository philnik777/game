import Elements.DebugInfoText;

import java.awt.*;
import java.util.LinkedList;

public class DebugPanel
{
    private LinkedList<Long> fpsData = new LinkedList<>();
    private LinkedList<Long> timingData = new LinkedList<>();

    private DebugInfoText fpsCounter = new DebugInfoText();
    private DebugInfoText frameCounter = new DebugInfoText();
    private DebugInfoText timePassed = new DebugInfoText();

    private DebugGraph fpsGraph = new DebugGraph(fpsData, 300, "FPS:");
    private DebugGraph timingGraph = new DebugGraph(timingData, 300, "Timings(ms):");

    private long frameBeginTime = System.nanoTime();
    private long frameCount = 0;

    private boolean show = true;
    private boolean showGraphs = true;

    private float timeMultiplier = 1;

    public DebugPanel()
    {
        fpsCounter.setPos(10, 20);
        frameCounter.setPos(10, 40);
        timePassed.setPos(10, 60);

        fpsGraph.setPos(10, 85);
        timingGraph.setPos(10, 255);
    }

    /*
     ** updates ups, sets new content for the DebugInfoTexts, inserts new data into LinkedLists for the graphs
     */
    public void updateUPS(long frameEndTime)
    {
        fpsCounter.setContent("   FPS: " + Double.valueOf(1000000000 / (frameEndTime - frameBeginTime)).longValue());
        frameCounter.setContent(" Frame: " + frameCount);
        timePassed.setContent("  Time: " + (frameEndTime - frameBeginTime) / 1000000 + "ms");

        fpsData.add(1000000000 / (frameEndTime - frameBeginTime));
        if (fpsData.size() > 300)
        {
            fpsData.removeFirst();
        }

        timingData.add((frameEndTime - frameBeginTime) / 1000000);
        if (timingData.size() > 300)
        {
            timingData.removeFirst();
        }
    }

    /*
     ** updates FPS, adds frame to frame counter
     */
    public void updateFPS()
    {
        long frameEndTime = System.nanoTime();
        updateUPS(frameEndTime);
        ++frameCount;
        timeMultiplier = (frameEndTime - frameBeginTime) / 1000000.f;
        frameBeginTime = System.nanoTime();
    }

    /*
     ** renders the debug numbers
     */
    private void renderNumbers(Graphics g)
    {
        fpsCounter.render(g);
        frameCounter.render(g);
        timePassed.render(g);
    }

    /*
     ** renders the debug graphs
     */
    private void renderGraphs(Graphics g)
    {
        fpsGraph.render(g);
        timingGraph.render(g);
    }

    public void render(Graphics g)
    {
        if (show)
        {
            renderNumbers(g);
        }
        if (showGraphs)
        {
            renderGraphs(g);
        }

    }

    public float getTimeMultiplier()
    {
        return timeMultiplier;
    }

    public void show(boolean show)
    {
        this.show = show;
    }

    public void showGraphs(boolean show)
    {
        this.showGraphs = show;
    }

    public boolean showGraphs()
    {
        return showGraphs;
    }
}
