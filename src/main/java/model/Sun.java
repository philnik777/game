package model;

import Elements.Position;
import Elements.RelativePosition;

import java.awt.*;
import java.util.Random;
import java.util.Vector;

public class Sun extends Unit
{

    private float time = 0;
    private UnitImage.InstanceType color;

    public Sun(UnitImage.InstanceType color)
    {
        super(UnitImage.getInstance(color));
        this.color = color;
    }

    @Override
    public void render(Graphics g)
    {
        Image sizedImage = getImage().getSizedImage();
        Position pos = getRelativePos().getAbsolutePos();
        g.drawImage(sizedImage, pos.x, pos.y, sizedImage.getWidth(null), sizedImage.getHeight(null), null);
    }

    private static final long spawnTime = 1000;

    public Spaceship[] produceSpaceships(float time)
    {
        Vector<Spaceship> spaceships = new Vector<>();
        this.time += time;

        if (this.time >= spawnTime)
        {

            int newSpaceships = (int) ((this.time - (this.time % spawnTime)) / spawnTime);
            this.time = this.time % spawnTime;

            for (int i = 1; i <= newSpaceships; i++)
            {
                Random random = new Random();
                Spaceship spaceship;

                if (this.color == UnitImage.InstanceType.SUN_1_BLUE)
                {
                    spaceship = new Spaceship(UnitImage.InstanceType.SPACESHIP_BLUE);
                } else
                {
                    spaceship = new Spaceship(UnitImage.InstanceType.SPACESHIP_RED);
                }


                RelativePosition relativePosition = new RelativePosition();

                float randomX = random.nextFloat();
                float randomY = random.nextFloat();

                while (randomX > 0.075f)
                {
                    randomX = random.nextFloat();
                }

                while (randomY > 0.075f)
                {
                    randomY = random.nextFloat();
                }

                relativePosition.setX(this.getRelativePos().getX() + randomX);
                relativePosition.setY(this.getRelativePos().getY() + randomY);
                spaceship.setRelativePos(relativePosition);

                spaceships.add(spaceship);
            }
        }

        return spaceships.toArray(new Spaceship[0]);
    }
}
