package model;

import Elements.Element2D;
import Elements.RelativePosition;

public abstract class Unit extends Element2D
{
    private RelativePosition pos = new RelativePosition();
    private UnitImage image = null;

    public Unit(UnitImage image)
    {
        this.image = image;
    }

    public RelativePosition getRelativePos()
    {
        return pos;
    }

    public void setRelativePos(RelativePosition pos)
    {
        this.pos = pos;
    }

    public UnitImage getImage()
    {
        return image;
    }
}
