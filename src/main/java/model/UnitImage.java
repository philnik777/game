package model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class UnitImage
{
    private BufferedImage originalImage = null;
    private Image sizedImage = null;
    private float relativeSize = 1;

    private static UnitImage spaceshipRedImage;
    private static UnitImage spaceshipBlueImage;
    private static UnitImage spaceshipWhiteImage;
    private static UnitImage sunRedImage;
    private static UnitImage sunBlueImage;
    private static long windowHeight = 1000;

    private static ClassLoader classLoader = ClassLoader.getSystemClassLoader();

    private UnitImage(BufferedImage image, float relativeSize)
    {
        this.relativeSize = relativeSize;
        originalImage = image;
        resize();
    }

    public static void setWindowSize(int width, int height)
    {
        windowHeight = height;
        if (spaceshipRedImage != null)
        {
            spaceshipRedImage.resize();
        }
        if (spaceshipBlueImage != null)
        {
            spaceshipBlueImage.resize();
        }
        if (spaceshipWhiteImage != null)
        {
            spaceshipWhiteImage.resize();
        }
        if (sunRedImage != null)
        {
            sunRedImage.resize();
        }
        if (sunBlueImage != null)
        {
            sunBlueImage.resize();
        }
    }

    private int getAbsoluteSize()
    {
        int ret = (int) (windowHeight * relativeSize);
        if (ret < 1)
        {
            ret = 1;
        }
        return ret;
    }

    private void resize()
    {
        sizedImage = originalImage.getScaledInstance(getAbsoluteSize(), getAbsoluteSize(), Image.SCALE_DEFAULT);
    }

    Image getSizedImage()
    {
        return sizedImage;
    }

    public enum InstanceType
    {
        SPACESHIP_RED,
        SPACESHIP_BLUE,
        SPACESHIP_WHITE,
        SUN_1_RED,
        SUN_1_BLUE
    }

    /*
     ** returns UnitImage for given Unit type
     */
    public static UnitImage getInstance(InstanceType type)
    {
        try
        {
            switch (type)
            {
                case SPACESHIP_RED:
                    if (spaceshipRedImage == null)
                    {
                        spaceshipRedImage = new UnitImage(ImageIO.read(classLoader.getResource("spaceship_red.png")), 0.03f);
                    }
                    return spaceshipRedImage;
                case SPACESHIP_BLUE:
                    if (spaceshipBlueImage == null)
                    {
                        spaceshipBlueImage = new UnitImage(ImageIO.read(classLoader.getResource("spaceship_blue.png")), 0.03f);
                    }
                    return spaceshipBlueImage;
                case SPACESHIP_WHITE:
                    if (spaceshipWhiteImage == null)
                    {
                        spaceshipWhiteImage = new UnitImage(ImageIO.read(classLoader.getResource("spaceship_white.png")), 0.03f);
                    }
                    return spaceshipWhiteImage;
                case SUN_1_RED:
                    if (sunRedImage == null)
                    {
                        sunRedImage = new UnitImage(ImageIO.read(classLoader.getResource("sun_lvl1_red.png")), 0.1f);
                    }
                    return sunRedImage;
                case SUN_1_BLUE:
                    if (sunBlueImage == null)
                    {
                        sunBlueImage = new UnitImage(ImageIO.read(classLoader.getResource("sun_lvl1_blue.png")), 0.1f);
                    }
                    return sunBlueImage;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
