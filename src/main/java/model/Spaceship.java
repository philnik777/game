package model;

import Elements.Position;

import java.awt.*;

public class Spaceship extends Unit
{

    public Spaceship(UnitImage.InstanceType color)
    {
        super(UnitImage.getInstance(color));
    }

    @Override
    public void render(Graphics g)
    {
        Image sizedImage = getImage().getSizedImage();
        Position pos = getRelativePos().getAbsolutePos();
        g.drawImage(sizedImage, pos.x, pos.y, sizedImage.getWidth(null), sizedImage.getHeight(null), null);
    }

    @Override
    public String toString()
    {
        return "Spaceship{}";
    }
}
