package Elements;

import java.awt.*;

public class DebugInfoText extends Element2D
{
    private String content;

    public DebugInfoText()
    {
        content = new String();
    }

    public DebugInfoText(String content)
    {
        this.content = content;
    }

    @Override
    public void render(Graphics g)
    {
        g.setFont(new Font("Courier New", Font.PLAIN, 12));
        g.drawString(content, pos.x, pos.y);
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }
}
