package Elements;

import java.awt.*;

public abstract class Element2D
{
    protected Position pos = new Position();
    String name = new String();

    /*
     ** renders the element
     */
    public abstract void render(Graphics g);

    public Position getPos()
    {
        return pos;
    }

    public void setPos(Position pos)
    {
        this.pos = pos;
    }

    public void setPos(int x, int y)
    {
        pos.x = x;
        pos.y = y;
    }
}
