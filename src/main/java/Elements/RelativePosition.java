package Elements;

public class RelativePosition
{
    private float x;
    private float y;

    private static int width = 0;
    private static int height = 0;

    public RelativePosition()
    {
        x = 0;
        y = 0;
    }

    public RelativePosition(float x, float y)
    {
        setX(x);
        setY(y);
    }

    public float getX()
    {
        return x;
    }

    public void setX(float x)
    {
        if (x <= 1.f && x >= 0.f)
        {
            this.x = x;
        }
    }

    public float getY()
    {
        return y;
    }

    public void setY(float y)
    {
        if (y <= 1.f && y >= 0.f)
        {
            this.y = y;
        }
    }

    public static void setWindowSize(int w, int h)
    {
        width = w;
        height = h;
    }

    public Position getAbsolutePos()
    {
        Position pos = new Position();
        pos.x = (int) (x * width);
        pos.y = (int) (y * height);
        return pos;
    }
}
